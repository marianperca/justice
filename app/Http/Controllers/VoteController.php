<?php

namespace App\Http\Controllers;

use DB;
use Illuminate\Http\Request;
use Log;

class VoteController extends Controller
{
    public function vote(Request $request)
    {
        $matricol    = $request->input('matricol');
        $vote_option = $request->input('option');

        if (empty($matricol) || empty($vote_option))
            return response()->json([
                'statusCode' => 400,
                'message' => 'Va rugam specificati toti parametrii!'
            ], 400);

        // check if user's ID follows a pattern
        if (!preg_match("/^31040701([A-Z]{2})([0-9]{6})$/i", $matricol))
            return response()->json([
                'statusCode' => 400,
                'message' => 'Numarul matricol nu este corect!'
            ], 400);

        // check if we already have a vote for this user
        $already_voted = DB::table('votes')->where('matricol', $matricol)->count();

        if ($already_voted > 0)
            return response()->json([
                'statusCode' => 400,
                'message' => 'Pentru acest nr. matricol s-a inregistrat deja un vot!'
            ], 400);

        if (!in_array($vote_option, [1, 2]))
            return response()->json([
                'statusCode' => 400,
                'message' => 'Vot incorect!'
            ], 400);

        // if we're here everything went fine => insert vote into database
        DB::table('votes')->insert([
            'matricol' => $matricol,
            'vote' => $vote_option,
        ]);

        return response()->json(['message' => 'Votul dvs. a fost inregistrat cu succes!']);
    }

    public function results()
    {
        $votes = DB::table('votes')
            ->select(DB::raw('count(*) as vote_count, vote'))
            ->groupBy('vote')
            ->pluck('vote_count', 'vote');

        return response()->json([
            1 => isset($votes[1]) ? intval($votes[1]) : 0,
            2 => isset($votes[2]) ? intval($votes[2]) : 0,
        ]);
    }
}
